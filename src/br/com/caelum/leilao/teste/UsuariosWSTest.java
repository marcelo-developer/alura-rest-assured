package br.com.caelum.leilao.teste;

import static com.jayway.restassured.RestAssured.given;
import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;

import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.path.json.JsonPath;
import com.jayway.restassured.path.xml.XmlPath;

import br.com.caelum.leilao.modelo.Usuario;

public class UsuariosWSTest {
	public static final Usuario MAURICIO;
	public static final Usuario GUILHERME;

	static {
		MAURICIO = new Usuario(1l, "Mauricio Aniche", "mauricio.aniche@caelum.com.br");
		GUILHERME = new Usuario(2l, "Guilherme Silveira", "guilherme.silveira@caelum.com.br");
	}
	
	@Test
	public void deveRetornarListaDeUsuarios() {
		XmlPath xmlPath = given()
				.header("Accept", "application/xml")
				.get("/usuarios")
				.xmlPath();
		
		List<Usuario> usuarios = xmlPath.getList("list.usuario", Usuario.class);
		assertEquals(2, usuarios.size());
		assertEquals(MAURICIO, usuarios.get(0));
		assertEquals(GUILHERME, usuarios.get(1));
	}
	
	@Test
	public void deveRetornarUsuarioPeloId() {
		JsonPath jsonPath = given()
				.header("Accept", "application/json")
				.parameter("usuario.id", 1)
				.get("/usuarios/show")
				.jsonPath();
		
		assertEquals(MAURICIO, jsonPath.getObject("usuario", Usuario.class));
	}
	
	@Test
	public void deveAdicionarEExcluirUmUsuario() {
		// Adiciona
		Usuario joao = new Usuario(3l, "Jo�o da Silva", "joao@caelum.com.br");
		Usuario usuarioCadastrado = given()
				.header("Accept", ContentType.XML.toString())
				.contentType(ContentType.XML)
				.body(joao)
			.expect()
				.statusCode(200)
			.when()
				.post("/usuarios")
			.andReturn()
				.xmlPath()
				.getObject("usuario", Usuario.class);
		
		assertEquals(joao.getNome(), usuarioCadastrado.getNome());
		assertEquals(joao.getEmail(), usuarioCadastrado.getEmail());
		
		// Exclui
		// Corrigido o servi�o pois n�o estava excluindo devido � compara��o das IDs (objetos Float) com ==
		given()
			.contentType(ContentType.XML)
			.body(usuarioCadastrado)
		.expect()
			.statusCode(200)
		.when()
			.delete("/usuarios/deleta");
		
		given()
			.header("Accept", "application/json")
			.parameter("usuario.id", usuarioCadastrado.getId())
		.expect()
			.statusCode(404)
		.when()
			.get("/usuarios/show");
	}
}
