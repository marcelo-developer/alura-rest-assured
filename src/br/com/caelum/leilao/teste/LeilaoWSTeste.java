package br.com.caelum.leilao.teste;

import static com.jayway.restassured.RestAssured.given;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.jayway.restassured.http.ContentType;

import br.com.caelum.leilao.modelo.Leilao;

public class LeilaoWSTeste {
	private static final Leilao GELADEIRA;
	
	static {
		GELADEIRA = new Leilao(1l, "Geladeira", 800.0, UsuariosWSTest.MAURICIO, false);
	}
	
	@Test
	public void deveRetornarLeilaoPeloId() {
		assertEquals(GELADEIRA, given()
				.header("Accept", "application/json")
				.parameter("leilao.id", 1)
				.get("/leiloes/show")
				.jsonPath()
				.getObject("leilao", Leilao.class));
	}
	
	@Test
	public void deveRetornarAQuantidadeDeLeiloes() {
		assertEquals(2, given()
				.header("Accept", "application/xml")
				.get("leiloes/total")
				.xmlPath()
				.getInt("int"));
	}
	
	@Test
	public void deveAdicionarEExcluirUmLeilao() {
		// Adiciona
		Leilao leilao = new Leilao(null, "Monalisa", 1530000.0, UsuariosWSTest.GUILHERME, false);
		Leilao leilaoCadastrado = given()
			.contentType(ContentType.XML)
			.header("Accept", ContentType.XML.toString())
			.body(leilao)
		.expect()
			.statusCode(200)
		.when()
			.post("/leiloes")
		.andReturn()
			.xmlPath()
			.getObject("leilao", Leilao.class);
		
		leilao.setId(leilaoCadastrado.getId());
		assertEquals(leilao, leilaoCadastrado);
		
		//Exclui
		// Corrigido o servi�o pois n�o estava excluindo devido � compara��o das IDs (objetos Float) com ==
		given()
			.contentType(ContentType.XML)
			.body(leilaoCadastrado)
		.expect()
			.statusCode(200)
		.when()
			.delete("/leiloes/deletar");

		// Verifica exclus�o
		given()
			.header("Accept", ContentType.XML.toString())
			.parameter("leilao.id", leilaoCadastrado.getId())
		.expect()
			.statusCode(404)
		.when()
			.get("/leiloes/show");
	}
}
